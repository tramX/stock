import {async, ComponentFixture, TestBed, inject} from '@angular/core/testing';

import {ProductListComponent} from './product-list.component';
import {ProductsService} from "../../stocks/stocks.service";
import {Router} from "@angular/router";
import {ConfirmationService, DialogModule, ConfirmDialogModule} from "primeng/primeng";
import {RouterTestingModule} from "@angular/router/testing";
import {FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators} from '@angular/forms';
import {HttpModule} from "@angular/http";
import {BrowserModule} from "@angular/platform-browser";




describe('ProductListComponent', () => {
  let component: ProductListComponent;
  let fixture: ComponentFixture<ProductListComponent>;
  let service;
  let router;
  let confirmationService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, DialogModule, ConfirmDialogModule, FormsModule, ReactiveFormsModule, HttpModule, BrowserModule, inject],
      declarations: [ProductListComponent],
      providers: [ProductsService, ConfirmationService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductListComponent);
    component = fixture.componentInstance;
    service = fixture.debugElement.injector.get(ProductsService);
    router = fixture.debugElement.injector.get(Router);
    confirmationService = fixture.debugElement.injector.get(ConfirmationService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
