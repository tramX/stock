import { Component, OnInit } from '@angular/core';
import {ProductsService} from "../products.service";
import {ConfirmationService, DialogModule} from "primeng/primeng";
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from "@angular/router";
import {DataTableModule, SharedModule} from 'primeng/primeng';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  products = [];
  addProductForm: FormGroup;
  display: boolean = false;

  constructor(private service: ProductsService, private router: Router, private confirmationService: ConfirmationService) {
  }

  ngOnInit() {
    this.addProductForm = new FormGroup({
      label : new FormControl('', [Validators.required])
    });
  }

  showAddForm(cd) {
    if (cd !== undefined){
      cd.reject();
    }
    this.display = true;
  }

  closeAddForm() {
    this.display = false;
  }

  addProduct(){
    this.service.addProduct(this.addProductForm.value.label);
    this.display = false;
    this.showMessage();

  }

  showMessage() {
    this.confirmationService.confirm({
      message: 'Продукт добавлен. Хотите добавить еще?',
      accept: () => {
      }
    });
  }


}
