import {Http} from "@angular/http";
import {Injectable} from "@angular/core";

interface IProduct {
  id: number;
  label: string;
}

@Injectable()
export class ProductsService {

  products: IProduct[];

  constructor(private http: Http){
    this.productList();
  }

  currentProduct = {};

  productList(){
    this.http.get('http://localhost:4200/assets/data.json')
      .subscribe(data => {
        for (const i of data.json()) {
          if (i.source === 'product'){
            this.products = i.data;
          }

        }
      });
  }

  getProduct(productId){

    this.products.forEach(function (eachObj) {
      if (eachObj.id == productId){
        this.currentProduct = eachObj;
      }

    }, this);

    return this.currentProduct;

  }

  updateProduct(productId, newLabel){

    this.products.forEach(function (eachObj) {
      if (eachObj.id == productId){
        eachObj.label = newLabel;
      }

    }, this);

  }

  addProduct(label: string){
    let newProductId = Math.max.apply(null, this.products.map(product => product.id))+1;
    this.products.push({'id': newProductId, 'label':label})
  }
}
