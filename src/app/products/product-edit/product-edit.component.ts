import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ProductsService} from "../products.service";
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.css']
})
export class ProductEditComponent implements OnInit {

  currentProduct = {};

  editProductForm: FormGroup;

  constructor(private route: ActivatedRoute,
              private service: ProductsService,
              private router: Router,
              ) {
  }

  ngOnInit() {

    this.currentProduct = this.service.getProduct(this.route.snapshot.params['id']);
    this.editProductForm = new FormGroup({
      label : new FormControl(this.currentProduct['label'], [Validators.required])
    });
  }

  updateProduct(){
    this.service.updateProduct(this.currentProduct['id'], this.editProductForm.value.label);
    this.router.navigate(['products/list']);
  }

}
