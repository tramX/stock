import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params} from "@angular/router";
import {ProductsService} from "../products.service";

@Component({
  selector: 'app-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.css']
})
export class ProductViewComponent implements OnInit {

  currentProduct = {};

  constructor(private route: ActivatedRoute, private service: ProductsService) {
  }

  ngOnInit() {
    this.currentProduct = this.service.getProduct(this.route.snapshot.params['id']);
  }

}
