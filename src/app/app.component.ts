import { Component, Input } from '@angular/core';

interface ITopMenu {
  title: string;
  url: string;
}

@Component({
  selector: 'app-root',
  template: `<div class="container">
    <nav style="margin-bottom: 20px;" class="navbar navbar-default">
      <div class="container-fluid">
        <ul class="nav navbar-nav">
          <li *ngFor="let item of topMenu" routerLinkActive="active">
            <a routerLink="{{item.url}}">{{item.title}}</a>
          </li>
        </ul>
      </div>
    </nav>
    <div class="row">
      <div class="col-xs-10 col-xs-offset-1">
        <router-outlet></router-outlet>
      </div>
    </div>
  </div>`,
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Stock';
  topMenu: ITopMenu[] = [{title:'Склады', url:'/stocks/list'}, {title:'Товары', url:'/products/list'}];
}
