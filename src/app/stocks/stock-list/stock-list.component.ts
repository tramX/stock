import { Component, OnInit } from '@angular/core';
import {StockService} from "../stocks.service";
import {Router} from "@angular/router";
import {ConfirmationService} from "primeng/primeng";

@Component({
  selector: 'app-stock-list',
  templateUrl: './stock-list.component.html',
  styleUrls: ['./stock-list.component.css']
})
export class StockListComponent implements OnInit {

  constructor(private service : StockService, private router: Router, private confirmationService: ConfirmationService) { }

  ngOnInit() {
  }

}
