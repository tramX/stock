import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ConfirmationService, DialogModule, DropdownModule, ButtonModule} from "primeng/primeng";
import {ActivatedRoute, Router} from "@angular/router";
import {StockService} from "../stocks.service";
import {ProductsService} from "../../products/products.service";
import { NgModule }      from '@angular/core';

@Component({
  selector: 'app-addinstock',
  templateUrl: './addinstock.component.html',
  styleUrls: ['./addinstock.component.css']
})
export class AddinstockComponent implements OnInit {

  addProductForm: FormGroup;
  display: boolean = true;
  products;
  selectedCar;

  constructor(private confirmationService: ConfirmationService,
              private service: StockService,
              private router: Router,
              private serviceProducts: ProductsService,
              private route: ActivatedRoute) { }

  ngOnInit() {

    this.addProductForm = new FormGroup({
      count: new FormControl('', [Validators.required, Validators.pattern('^[0-9]*$')]),
      price: new FormControl('', [Validators.required, Validators.pattern('^[0-9]*$')]),
      selectedProduct: new FormControl('', [])
    });


  }

  addInStock(selectedProduct){
    for (const stock of this.service.stocks){
      if (stock.id == this.route.snapshot.params['stock_id']){
        if (stock.products[selectedProduct.selectedOption.id]){
          stock.products[selectedProduct.selectedOption.id]['count'] = parseInt(this.addProductForm.value.count);
          stock.products[selectedProduct.selectedOption.id]['price'] = parseInt(this.addProductForm.value.price);
          this.router.navigate(['stocks/:id/view'], { queryParams: { id: stock.id } });
        }
        else{
          stock.products[selectedProduct.selectedOption.id] = {"id": parseInt(selectedProduct.selectedOption.id),
            "label": selectedProduct.selectedOption.label,
            "count": parseInt(this.addProductForm.value.count),
            "price": parseInt(this.addProductForm.value.price)}
          this.router.navigate(['stocks/:id/view'], { queryParams: { id: stock.id } });
        }
      }

    }
  }

}
