import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddinstockComponent } from './addinstock.component';

describe('AddinstockComponent', () => {
  let component: AddinstockComponent;
  let fixture: ComponentFixture<AddinstockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddinstockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddinstockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
