import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'productsinstock'
})
export class ProductsinstockPipe implements PipeTransform {

  transform(value: any, args?: any): any {

    let products_count = 0;

    for (const key of Object.keys(value.products)){
      products_count = products_count + value.products[key].count;
    }

    return products_count;
  }

}
