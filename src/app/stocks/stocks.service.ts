import {Http} from "@angular/http";
import {Injectable} from "@angular/core";

interface IStock {
  id: number;
  label: string;
  products: {};
}

@Injectable()
export class StockService {

  stocks: IStock[];

  currentStock: {};

  constructor(private http: Http){
    this.stockList();
  }

  stockList(){
    this.http.get('http://localhost:4200/assets/data.json')
      .subscribe(data => {
        for (const i of data.json()) {
          if (i.source === 'stock'){
            this.stocks = i.data;
          }

        }
      });
  }

  getStock(stockId){
    for (const stock of this.stocks){
      if (stock.id == stockId){
        this.currentStock = stock;
      }
    }

    return this.currentStock;

  }

}
