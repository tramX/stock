import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params} from "@angular/router";
import {StockService} from "../stocks.service";
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-stock-view',
  templateUrl: './stock-view.component.html',
  styleUrls: ['./stock-view.component.css']
})
export class StockViewComponent implements OnInit {

  currentStock = {};
  productsInStock = [];

  constructor(private route: ActivatedRoute, private service: StockService) {
  }

  ngOnInit() {

    this.currentStock = this.service.getStock(this.route.snapshot.params['id']);
    this.productsInStock = Object.values(this.currentStock['products']);
  }

}
