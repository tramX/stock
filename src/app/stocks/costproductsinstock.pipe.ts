import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'costproductsinstock'
})
export class CostproductsinstockPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let cost_products = 0;

    for (const key of Object.keys(value.products)){
      cost_products = cost_products + (value.products[key].count * value.products[key].price);
    }

    return cost_products;
  }

}
