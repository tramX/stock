import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { StocksComponent } from './stocks/stocks.component';
import { StockViewComponent } from './stocks/stock-view/stock-view.component';
import { StockListComponent } from './stocks/stock-list/stock-list.component';

import { ProductsComponent } from './products/products.component';
import { ProductListComponent } from './products/product-list/product-list.component';
import { ProductViewComponent } from './products/product-view/product-view.component';

import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {AppRoutingModule} from "./app-routing.module";
import { ProductEditComponent } from './products/product-edit/product-edit.component';
import {ProductsService} from "./products/products.service";

import { BootstrapModalModule } from 'ngx-modialog/plugins/bootstrap';
import { ProductAddComponent } from './products/product-add/product-add.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {
  ConfirmDialogModule, ConfirmationService, DialogModule, DataTableModule, SharedModule,
  DropdownModule, ButtonModule
} from 'primeng/primeng';
import {StockService} from "./stocks/stocks.service";
import { ProductsinstockPipe } from './stocks/productsinstock.pipe';
import { CostproductsinstockPipe } from './stocks/costproductsinstock.pipe';
import { AddinstockComponent } from './stocks/addinstock/addinstock.component';




@NgModule({
  declarations: [
    AppComponent,
    StocksComponent,
    ProductsComponent,
    ProductListComponent,
    ProductViewComponent,
    StockViewComponent,
    StockListComponent,
    ProductEditComponent,
    ProductAddComponent,
    ProductsinstockPipe,
    CostproductsinstockPipe,
    AddinstockComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BootstrapModalModule,
    ConfirmDialogModule,
    DialogModule,
    DataTableModule,
    SharedModule,
    DropdownModule,
    ButtonModule
  ],
  providers: [ProductsService, ConfirmationService, StockService],
  bootstrap: [AppComponent]
})
export class AppModule { }
