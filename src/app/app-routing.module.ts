import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import {StockListComponent} from "./stocks/stock-list/stock-list.component";
import {ProductListComponent} from "./products/product-list/product-list.component";
import {AppComponent} from "./app.component";
import {ProductViewComponent} from "./products/product-view/product-view.component";
import {ProductEditComponent} from "./products/product-edit/product-edit.component";
import {ProductsComponent} from "./products/products.component";
import {StockViewComponent} from "./stocks/stock-view/stock-view.component";
import {AddinstockComponent} from "./stocks/addinstock/addinstock.component";

const routes: Routes = [
  {path: 'stock/:stock_id/add', component: AddinstockComponent},
  {path: 'stocks/list', component: StockListComponent},
  {path: 'stocks/:id/view', component:StockViewComponent},
  {path: 'products/list', component:ProductListComponent},
  {path: 'products/:id/view', component:ProductViewComponent},
  {path: 'products/:id/edit', component:ProductEditComponent}
];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})

export class AppRoutingModule {
}
